<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

use App\Service;

class ServiceController extends Controller
{
    function index(){


        return  view('admin.frontEnd.homeSettings.services');

//
    }


    public function createService(Request $request){

        $service = Service::create([
            'title' => $request->service['title'],
            'description' => $request->service['description'],
        ]);




        return redirect('/admin/service')->with('success', 'Save Successfully!');
;

    }


    public function serviceInfo(){
        $services = Service::get();

        return response()->json(["services"=>$services]);

    }


    public function deleteServiceInfo(Request $request){

        $services = Service::find($request->id);
        $services->delete();
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public  function serviceUpdate(Request $request){


       $service = Service::update(array(

           'title' => $request->service['title'],
           'description' => $request->service['description'],
       ));



        return redirect('/admin/service');


    }
}
