<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Candidate;
use App\ProjectSetting;
use Auth;

class CandidateController extends Controller
{
	public function infoAdmin()
	{
		return view('admin.candidate.info');
	}
    public function infoEmployee()
    {
        return view('employee.candidate.info');
    }
    public function candidatesAll()
    {
        $candidates = Candidate::all();
        return response()->json(["candidates"=>$candidates]);
    }
	public function info()
	{
        $setting = ProjectSetting::where('key', '=', 'interview-link')->first();

        if($setting && $setting->is_active) {
            return view('common.candidate.info');
        } else {
            return redirect()->route('page404');
        }
	}

    public function addOrUpdate(Request $request)
    {
        //validate data
        $validator = \Validator::make($request->candidate, [
            'name'=>'required|string',
            'phone'=>'required',
            'email'=>'required|email',
            'expected_salary'=>'required|numeric',
            'expected_join_date'=>'required|date',
            'expertise'=>'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' =>false , 'errors'=>$validator->messages()]);
        }

        if($request->candidate['id'] == null){
            // create
            $candidate = Candidate::create([
                'name' => $request->candidate['name'],
                'phone' => $request->candidate['phone'],
                'email' => $request->candidate['email'],
                'expected_salary' => $request->candidate['expected_salary'],
                'expected_join_date' => $request->candidate['expected_join_date'],
                'expertise' => $request->candidate['expertise'],
            ]);

            $candidate = Candidate::find($candidate->id);
            return response()->json(["success"=>true, 'status'=>'created', 'candidate'=>$candidate]);
        } else {      
            //update
            $candidate = candidate::find($request->candidate['id']);
            if(!$candidate) return response()->json(["success"=>true, 'status'=>'somethingwrong']);
            if(Auth::guard('admin')->check()) {
                $candidate->update([
                    'name' => $request->candidate['name'],
                    'phone' => $request->candidate['phone'],
                    'email' => $request->candidate['email'],
                    'expected_salary' => $request->candidate['expected_salary'],
                    'expected_join_date' => $request->candidate['expected_join_date'],
                    'expertise' => $request->candidate['expertise'],
                ]);
            }
            return response()->json(["success"=>true, 'status'=>'updated', 'candidate'=>$candidate]);
        }
    }
}
