<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Interview;
use Auth;

class InterviewController extends Controller
{
	public function infoAdmin()
	{
		return view('admin.candidate.interview');
	}
	public function infoEmployee()
	{
		return view('employee.candidate.interview');
	}
    public function interviewsAll()
    {
        $interviews = Interview::with('candidate', 'creator');
        // if(Auth::guard('employee')->check()) 
        	// $interviews=$interviews->where('created_by', '=', Auth::guard('employee')->user()->id);
        $interviews = $interviews->get();
        return response()->json(["interviews"=>$interviews]);
    }
    public function addOrUpdate(Request $request)
    {
        //validate data
        $validator = \Validator::make($request->interview, [
            'candidate.id'=>'required|numeric',
            'date'=>'required|date',
            'test_task'=>'required|string',
            'output'=>'nullable|string',
            'mark'=>'nullable|numeric|min:0|max:10',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' =>false , 'errors'=>$validator->messages()]);
        }

        if($request->interview['id'] == null){
            // create
            $interview = Interview::create([
                'candidate_id'=>$request->interview['candidate']['id'],
                'date'=>$request->interview['date'],
                'test_task'=>$request->interview['test_task'],
                'output'=>$request->interview['output'],
                'mark'=>$request->interview['mark'],
            ]);

            $interview = Interview::with('candidate')->where('id','=',$interview->id)->first();
            return response()->json(["success"=>true, 'status'=>'created', 'interview'=>$interview]);
        } else {      
            //update
            $interview = Interview::with('candidate')->where('id','=',$request->interview['id'])->first();
            if(!$interview) return response()->json(["success"=>true, 'status'=>'somethingwrong']);
            if(Auth::guard('admin')->check()) {
                $interview->update([
                    'candidate_id'=>$request->interview['candidate']['id'],
                    'date'=>$request->interview['date'],
                    'test_task'=>$request->interview['test_task'],
                    'output'=>$request->interview['output'],
                    'mark'=>$request->interview['mark'],
                ]);

                $interview = Interview::with('candidate')->where('id','=',$interview->id)->first();
            }
            return response()->json(["success"=>true, 'status'=>'updated', 'interview'=>$interview]);
        }
    }
}
