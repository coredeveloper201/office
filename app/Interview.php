<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interview extends Model
{
	protected $fillable = [
        'candidate_id', 'test_task', 'date', 'output', 'mark', 'created_by'
    ];

    public function candidate()
    {
        return $this->belongsTo(Candidate::class);
    }

    public function creator()
    {
        return $this->hasOne(Employee::class, 'id', 'created_by');
    }
}