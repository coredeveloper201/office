<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
	protected $fillable = [
        'name', 'phone', 'email', 'expected_salary', 'expected_join_date', 'expertise', 'attached',
    ];

    public function interviews()
    {
        return $this->hasMany(Interview::class);
    }
}