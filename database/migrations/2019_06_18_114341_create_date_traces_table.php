<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDateTracesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('date_traces', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date')->nullable();
            $table->integer('user_id')->unsigned();
            $table->integer('application_id')->unsigned()->nullable();
            $table->integer('created_by')->unsigned()->nullable(); //any admin or system (employee cannot create auto create will be system.)
            $table->enum('status',['workday','weekend','government-holiday','private-holiday','sickness','not-defined','custom'])->default('not-defined');
            $table->boolean('is_holiday')->default(false);
            $table->string('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('date_traces');
    }
}
