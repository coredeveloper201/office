<?php

use Illuminate\Database\Seeder;

class InterviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$faker = \Faker\Factory::create();

    	foreach (range(1,10) as $index) {
    		$candidate = App\Candidate::create([
                'name'=>$faker->name,
                'email'=>$faker->email,
                'phone'=>'01800000000',
                'expected_salary'=>50000,
                'expected_join_date'=>today(),
                'expertise'=>'php, laravel, bootstrap',
                'attached'=>'default/images/1.jpg',
            ]);
            if($index%2 == 1) {
	    		$interview = App\Interview::create([
	                'candidate_id'=>$candidate->id,
	                'test_task'=>$faker->text($maxNbChars = 100),
	                'date'=>today(),
	                'output'=>$faker->text($maxNbChars = 100),
	                'mark'=>$index,
                    'created_by'=>$index%3? 0:1,
	            ]);
	    	}
		}
        echo "Interview seeded successfully\n";
    }
}
