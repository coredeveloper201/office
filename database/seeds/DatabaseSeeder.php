<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        echo "Database seeding .... \n";
        $this->call(AuthenticateSeeder::class);
        $this->call(InterviewSeeder::class);
        $this->call(DateTraceTableSeeder::class);

        echo "Database seeded .... \n";
        echo "need to seed application and date trace.\n";
    }
}
