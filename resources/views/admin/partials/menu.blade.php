<nav class="sidebar-nav">
    <ul id="sidebarnav">
        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="starter-kit.html" aria-expanded="false"><i class="far fa-lightbulb"></i><span class="hide-menu">Link Type</span></a></li>
        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('employees') }}" aria-expanded="false"><i class="fas fa-user-plus"></i><span class="hide-menu">Employee</span></a></li>
        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fas fa-compress"></i><span class="hide-menu">Interview</span></a>
            <ul aria-expanded="false" class="collapse first-level">
                <li class="sidebar-item"><a href="{{ route('admin.interview') }}" class="sidebar-link"><i class="mdi mdi-octagram"></i><span class="hide-menu"> Interviews</span></a></li>
                <li class="sidebar-item"><a href="{{ route('admin.candidate') }}" class="sidebar-link"><i class="mdi mdi-octagram"></i><span class="hide-menu"> Candidate Info add</span></a></li>
                <li class="sidebar-item"><a href="{{ route('interview.setting') }}" class="sidebar-link"><i class="mdi mdi-octagram"></i><span class="hide-menu"> Setting</span></a></li>
            </ul>
        </li>
        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fas fa-compress"></i><span class="hide-menu">Dropdown Link</span></a>
            <ul aria-expanded="false" class="collapse first-level">
                <li class="sidebar-item"><a href="javascript:void(0)" class="sidebar-link"><i class="mdi mdi-octagram"></i><span class="hide-menu"> Second Level Item</span></a></li>
                <li class="sidebar-item"><a href="javascript:void(0)" class="sidebar-link"><i class="mdi mdi-octagram"></i><span class="hide-menu"> Second Level Item</span></a></li>
            </ul>
        </li>

        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fas fa-compress"></i><span class="hide-menu">Home Settings</span></a>
            <ul aria-expanded="false" class="collapse first-level">
                <li class="sidebar-item"><a href="{{ route('admin.slider') }}" class="sidebar-link"><i class="mdi mdi-octagram"></i><span class="hide-menu"> Slider</span></a></li>
                <li class="sidebar-item"><a href="{{ route('admin.service') }}" class="sidebar-link"><i class="mdi mdi-octagram"></i><span class="hide-menu"> Services</span></a></li>

            </ul>
        </li>



    </ul>
</nav>